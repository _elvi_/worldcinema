package com.example.worldcinema.sign_up_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.worldcinema.R
import com.example.worldcinema.databinding.ActivitySignUpBinding
import com.example.worldcinema.main_activity.MainActivity
import com.example.worldcinema.sign_in_screen.SignInActivity

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.enter.setOnClickListener {
            val name = binding.nameEditText.text.toString()
            val surName = binding.surnameEditText.text.toString()
            val email = binding.emailEditText.text.toString()
            val password = binding.passwordEditText.text.toString()
            val repeatPassword = binding.repeatPasswordEditText.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty() && name.isNotEmpty() && surName.isNotEmpty() && repeatPassword.isNotEmpty()) {
                startActivity(
                    Intent(this, MainActivity::class.java)
                )
                finish()
            } else Toast.makeText(this, getString(R.string.stringCheckLines), Toast.LENGTH_SHORT).show()
        }

        binding.registration.setOnClickListener{
            startActivity(
                Intent(this, SignInActivity::class.java)
            )
            finish()
        }
    }
}