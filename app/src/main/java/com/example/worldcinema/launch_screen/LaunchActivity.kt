package com.example.worldcinema.launch_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.worldcinema.databinding.ActivityLaunchBinding
import com.example.worldcinema.sign_in_screen.SignInActivity
import com.example.worldcinema.sign_up_screen.SignUpActivity

class LaunchActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLaunchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLaunchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Handler().postDelayed({
            startActivity(
                Intent(this, SignUpActivity::class.java)
            )
            finish()
        }, 3000)
    }
}